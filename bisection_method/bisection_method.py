# -*- coding: utf-8 -*-
"""
    This module is an implementation of the bisection method
"""

from math import sin, cos, tan, asin, acos, atan, log, exp


def bisection_method(function_x, a, b, precision):
    """
        This Calculates root Approximation of Functions
        Args:
            function_x (function): Function.
            a (float): Interval 1.
            b (float): Interval 2.
            precision (float): Desired precision for an approximation.
        Returns:
            Truple with root approximation of function_x and steps
    """
    if (function_x(a) * function_x(b)) >= 0:
        # (>= 0) If the root is a or b it will not be calculated
        return None, None
    p = (a + b) / 2.0
    steps = 0
    while abs(function_x(p)) > precision:
        steps += 1
        if (function_x(p) * function_x(a)) < 0:
            b = p
        else:
            a = p
        p = (a + b) / 2.0
    return (p, steps)


def main():
    """
        Receives terminal function and approximates
        using the bisection method
    """
    fx_input = input("Função(x):")
    fx = lambda x: eval(fx_input)
    a = float(input("Intervalo A:"))
    b = float(input("Intervalo B:"))
    precision = float(input("Precisão:"))
    result = bisection_method(fx, a, b, precision)
    print("Aproximaçaõ:", result[0])
    print("Qt. Etapas:", result[1])

if __name__ == '__main__':
    main()