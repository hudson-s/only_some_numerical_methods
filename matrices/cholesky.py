# -*- coding: utf-8 -*-
"""
    This module is an implementation of the Cholesky decomposition
"""


def Gt(G):
    """
        Module calculates the Matrix transposed
        Args:
            G (Matrix): Matrix
        Returns:
            Matrix transposed
    """
    order = len(G)
    GT = [[0.0] * order for i in range(order)]
    for i in range(order):
        for j in range(order):
            GT[i][j] = G[j][i]
    return GT


def cholesky(A):
    """
        Methods for decomposition of matrices.
        Args:
            A (matrix): Matrix for decomposition
    """
    order = len(A)
    G = [[0.0] * order for i in range(order)]

    for i in range(order):
        for j in range(i+1):
            addition = 0
            for k in range(j):
                addition += (G[i][k] * G[j][k])
            # Calculate Gii - The formula fits for e i==j
            if i == j:
                G[i][j] = (A[i][i] - addition) ** (1/2.0)
            # Calculate Gij
            else:
                G[i][j] = ((1 / G[j][j]) * float(A[i][j] - addition))
    GT = Gt(G)
    print("Matriz G:")
    for i in range(len(G)):
        for j in range(len(G)):
            print(G[i][j], end=' ')
        print()
    print()
    print("Matriz Gt:")
    for i in range(len(G)):
        for j in range(len(G)):
            print(GT[i][j], end=' ')
        print()
    print()


def main():
    """
        It receives matrix for decomposition
    """
    order = int(input("Digite a ordem: "))
    A = range(order)
    A = [range(order) for x in A]

    print("Digite a matriz:")
    for i in range(order):
        A[i] = input()
        A[i] = list(map(float, A[i].split()))

    cholesky(A)

if __name__ == '__main__':
    main()
